using System;
using UnityEngine;

namespace Quinmirn.UI.DialogBoxes.Samples
{
    public sealed class ShowDialogBoxExample : MonoBehaviour
    {
        [SerializeField] private string _title = "My Title";

        [SerializeField] private string _message = "My Message";

        [SerializeField] private DialogBoxActionDefinition[] _actions = Array.Empty<DialogBoxActionDefinition>();

        private void Start()
        {

            var builder = UIDialogBuilder.Create()
                .SetTitle(_title)
                .SetMessage(_message);

            foreach (var action in _actions)
            {
                builder.AddAction(action.Title, action.Action.Invoke);
            }
            
            builder.Build();

            Debug.Log("DialogBox created! ");
        }
    }
}

