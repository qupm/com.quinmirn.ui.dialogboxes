﻿using System;
using UnityEngine.Events;

namespace Quinmirn.UI.DialogBoxes.Samples
{
    [Serializable]
    public sealed class DialogBoxActionDefinition
    {
        public string Title = "Action";
        
        public UnityEvent Action = new();
    }
}