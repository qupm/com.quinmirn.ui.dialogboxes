# UI Dialog Boxes

Show Dialog Box in Unity.

## Installation

### Requirement

* Unity 2021.3 or later

### Using Unity Packager Manager

``` json
{
    "dependencies": {
        "com.quinmirn.ui.dialogboxes": "https://gitlab.com/qupm/com.quinmirn.ui.dialogboxes.git"
    }
}
```

## Setup


Before you start writing a single line of code, make sure to setup the settings first:

* In file menu, open `Q/UI/Dialog Boxes/Open Settings...`

This will open (and create) the settings asset where you can configure the prefab of the dialog.

You can import the `Cube or Sphere?` sample for a sample prefab.

## Usage

Here's a sample code of how to create a dialog box.

``` c#
UIDialogBuilder.Create()
  .SetTitle("My Title")
  .SetMessage("My Message")
  .AddAction("Action 1", () => 
  {
    // Action 1
  })
  .AddAction("Action 2", () =>
  {
    // Action 2
  })
  .Build();
```
