﻿using System.Collections.Generic;
using UnityEngine;

namespace Quinmirn.UI.DialogBoxes.Components
{
    [DisallowMultipleComponent]
    public sealed class UIDialogBox : MonoBehaviour, IUIDialogBox
    {
        [SerializeField] private UIDialogBoxLabel _titleLabel;

        [SerializeField] private UIDialogBoxLabel _messageLabel;

        [SerializeField] private UIDialogBoxActionButton _actionButtonTemplate;

        private void Awake()
        {
            // Make sure to deactivate the action button template.
            _actionButtonTemplate.gameObject.SetActive(false);
        }

        private void Dismiss()
        {
            Destroy(gameObject);
        }

        public IUIDialogBox SetTitle(string title)
        {
            _titleLabel.SetText(title);

            var showTitle = !string.IsNullOrWhiteSpace(title);
            _titleLabel.gameObject.SetActive(showTitle);

            return this;
        }

        public IUIDialogBox SetMessage(string message)
        {
            _messageLabel.SetText(message);

            var showMessage = !string.IsNullOrWhiteSpace(message);
            _messageLabel.gameObject.SetActive(showMessage);

            return this;
        }

        public IUIDialogBox SetActions(IReadOnlyList<UIDialogBoxAction> actions)
        {
            CreateActionButtons(actions);
            
            return this;
        }

        private void CreateActionButtons(IReadOnlyList<UIDialogBoxAction> actions)
        {
            if (actions == null)
            {
                return;
            }
            
            var actionCount = actions.Count;

            for (var i = 0; i < actionCount; i++)
            {
                var action = actions[i]; 
                CreateActionButton(action);
            }
        }

        private void CreateActionButton(UIDialogBoxAction action)
        {
            if (action == null)
            {
                return;
            }
            
            var actionButton = Instantiate(_actionButtonTemplate, _actionButtonTemplate.transform.parent);
            actionButton.SetTitle(action.Title);
            actionButton.SetCallback(() =>
            {
                action.Callback?.Invoke();

                Dismiss();
            });
            
            actionButton.gameObject.SetActive(true);
        }
    }
}
