﻿using UnityEngine;
using UnityEngine.Events;

namespace Quinmirn.UI.DialogBoxes.Components
{
    [DisallowMultipleComponent]
    public sealed class UIDialogBoxLabel : MonoBehaviour
    {
        public UnityEvent<string> TextUpdated = new();

        public void SetText(string text)
        {
            TextUpdated.Invoke(text);
        }
    }
}