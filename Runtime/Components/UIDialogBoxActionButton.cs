﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Quinmirn.UI.DialogBoxes.Components
{
    [DisallowMultipleComponent]
    public sealed class UIDialogBoxActionButton : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private UIDialogBoxLabel _titleLabel;

        private Action _callback;

        private void OnDestroy()
        {
            _callback = null;
        }

        public void SetTitle(string title)
        {
            _titleLabel.SetText(title);
        }

        public void SetCallback(Action callback)
        {
            _callback = callback;
        }

        void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
        {
            _callback?.Invoke();
        }
    }
}
