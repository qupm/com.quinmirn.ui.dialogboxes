﻿using System;
using System.Collections.Generic;
using UnityEngine.Pool;
using Object = UnityEngine.Object;

namespace Quinmirn.UI.DialogBoxes
{
    internal sealed class UIDialogBuilderPool
    {
        private readonly ObjectPool<UIDialogBuilder> _pool;

        public UIDialogBuilderPool()
        {
            _pool = new ObjectPool<UIDialogBuilder>(CreateBuilder, OnGetBuilder, OnReleaseBuilder, OnDestroyBuilder,
                collectionCheck: true, defaultCapacity: 1, maxSize: 3);
        }

        public UIDialogBuilder Get()
        {
            return _pool.Get();
        }

        public void Release(UIDialogBuilder builder)
        {
            _pool.Release(builder);
        }

        private UIDialogBuilder CreateBuilder()
        {
            return new UIDialogBuilder();
        }

        private void OnGetBuilder(UIDialogBuilder builder)
        {
            
        }

        private void OnReleaseBuilder(UIDialogBuilder builder)
        {
            builder.CleanUp();
        }

        private void OnDestroyBuilder(UIDialogBuilder builder)
        {
            builder.Dispose();
        }
    }
    public sealed class UIDialogBuilder : IDisposable
    {
        private static UIDialogBoxBuildSettingsAsset SETTINGS { get; }
        
        private static UIDialogBuilderPool BUILDER_POOL { get; }
        
        static UIDialogBuilder()
        {
            SETTINGS = UIDialogBoxBuildSettingsAsset.Load();

            BUILDER_POOL = new UIDialogBuilderPool();
        }
        
        public static UIDialogBuilder Create()
        {
            return BUILDER_POOL.Get();
        }

        private string _title;

        private string _message;

        private List<UIDialogBoxAction> _actions;

        public void Dispose()
        {
            CleanUp();
        }

        public void CleanUp()
        {
            if (_actions != null)
            {
                _actions.Clear();
            }
        }

        public UIDialogBuilder SetTitle(string title)
        {
            _title = title;
            
            return this;
        }
        
        public UIDialogBuilder SetMessage(string message)
        {
            _message = message;
            
            return this;
        }
        
        public UIDialogBuilder AddAction(string title, Action callback)
        {
            if (_actions == null)
            {
                _actions = new List<UIDialogBoxAction>();
            }

            var action = new UIDialogBoxAction(title, callback);
            _actions.Add(action);
            
            return this;
        }

        public IUIDialogBox Build()
        {
            var dialogBox =  Object.Instantiate(SETTINGS.Prefab)
                .SetTitle(_title)
                .SetMessage(_message)
                .SetActions(_actions);

            BUILDER_POOL.Release(this);

            return dialogBox;
        }
    }
}