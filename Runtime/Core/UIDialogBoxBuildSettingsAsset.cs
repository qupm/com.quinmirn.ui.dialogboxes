﻿using Quinmirn.UI.DialogBoxes.Components;
using UnityEngine;

namespace Quinmirn.UI.DialogBoxes
{
    public sealed class UIDialogBoxBuildSettingsAsset : ScriptableObject
    {
        [Header("General Settings")]
        [SerializeField] private UIDialogBox _prefab;

        public UIDialogBox Prefab => _prefab;

        private static readonly string RESOURCE_PATH = $"UI/{nameof(UIDialogBoxBuildSettingsAsset)}";

        internal static UIDialogBoxBuildSettingsAsset Load()
        {
            var settings = Resources.Load<UIDialogBoxBuildSettingsAsset>(RESOURCE_PATH);
            if (settings == null)
            {
                Debug.LogError("#UIDialogBoxes# Failed to load settings asset.");
            }
            
            return settings;
        }

#if UNITY_EDITOR
        
        [UnityEditor.MenuItem("Q/UI/Dialog Boxes/Open Settings...")]
        internal static void OpenSettingsAsset()
        {
            var path = $"Assets/Q/Resources/{RESOURCE_PATH}.asset";
            var settings = UnityEditor.AssetDatabase.LoadAssetAtPath<UIDialogBoxBuildSettingsAsset>(path);
            if (settings == null)
            {
                settings = CreateInstance<UIDialogBoxBuildSettingsAsset>();

                System.IO.Directory.CreateDirectory(path);

                UnityEditor.AssetDatabase.CreateAsset(settings, path);

                UnityEditor.AssetDatabase.ImportAsset(path);
            }

            UnityEditor.AssetDatabase.OpenAsset(settings);
        }
        
#endif
    }
}