﻿using System.Collections.Generic;

namespace Quinmirn.UI.DialogBoxes
{
    public interface IUIDialogBox
    {
        IUIDialogBox SetTitle(string title);

        IUIDialogBox SetMessage(string message);
        
        IUIDialogBox SetActions(IReadOnlyList<UIDialogBoxAction> actions);

        T GetComponent<T>();

        bool TryGetComponent<T>(out T component);
    }
}
