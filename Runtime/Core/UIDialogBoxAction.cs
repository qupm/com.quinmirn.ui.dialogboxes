﻿using System;

namespace Quinmirn.UI.DialogBoxes
{
    public sealed class UIDialogBoxAction
    {
        public string Title { get; }

        public Action Callback { get; }

        public UIDialogBoxAction(string title, Action callback)
        {
            Title = title;
            Callback = callback;
        }
    }
}